extern CFArrayRef SBSCopyApplicationDisplayIdentifiers(bool active,bool internal);
extern CFStringRef SBSCopyLocalizedApplicationNameForDisplayIdentifier(CFStringRef appID);
extern int SBSLaunchApplicationForDebugging(CFStringRef appID,CFURLRef URL,CFArrayRef args,CFDictionaryRef env,CFStringRef stdout,CFStringRef stderr,char flags);
extern bool SBSProcessIDForDisplayIdentifier(CFStringRef appID,pid_t *pid);
extern CFStringRef SBSApplicationLaunchingErrorString(int error);
extern void SBSOpenSensitiveURLAndUnlock(CFURLRef URL,bool unlock);
extern mach_port_t SBSSpringBoardServerPort();
extern void SBBundlePathForDisplayIdentifier(mach_port_t port,const char* appID,char* path);
extern void SBFrontmostApplicationDisplayIdentifier(mach_port_t port,char* appID);

static CFStringRef $_copyAppID(const char* _app) {
  CFStringRef appID=CFStringCreateWithCStringNoCopy(NULL,_app,kCFStringEncodingUTF8,kCFAllocatorNull);
  CFArrayRef appIDs=SBSCopyApplicationDisplayIdentifiers(false,false);
  CFIndex len=CFArrayGetCount(appIDs),i;
  for (i=0;i<len;i++){
    CFStringRef ID=CFArrayGetValueAtIndex(appIDs,i),
     name=SBSCopyLocalizedApplicationNameForDisplayIdentifier(ID);
    CFComparisonResult cmp=CFStringCompare(name,appID,kCFCompareCaseInsensitive|kCFCompareDiacriticInsensitive);
    CFRelease(name);
    if(cmp==kCFCompareEqualTo){
      CFRelease(appID);
      appID=CFRetain(ID);
      break;
    }
  }
  CFRelease(appIDs);
  return appID;
}
static CFStringRef $_copyPath(const char* _path) {
  CFURLRef URL=CFURLCreateFromFileSystemRepresentation(NULL,(void*)_path,strlen(_path),false),
   absURL=CFURLCopyAbsoluteURL(URL);
  CFRelease(URL);
  CFStringRef path=CFURLCopyFileSystemPath(absURL,kCFURLPOSIXPathStyle);
  CFRelease(absURL);
  return path;
}
static char* $_copyUTF8String(CFStringRef str) {
  CFIndex size=CFStringGetMaximumSizeForEncoding(CFStringGetLength(str),kCFStringEncodingUTF8);
  char* _str=malloc(size);
  CFStringGetCString(str,_str,size,kCFStringEncodingUTF8);
  return _str;
}
struct app {
  CFStringRef name,ID;
};
static int $_compare(const void* A,const void* B) {
  return CFStringCompare(*(CFStringRef*)A,*(CFStringRef*)B,kCFCompareCaseInsensitive|kCFCompareDiacriticInsensitive);
}

int main(int argc,char** argv) {
  const char* _outFile=NULL;
  const char* _errFile=NULL;
  CFURLRef openURL=NULL;
  char buf[1024];
  int opt;
  while((opt=getopt(argc,argv,"lqo:e:u:"))!=-1){
    if(opt=='l'){
      CFArrayRef appIDs=SBSCopyApplicationDisplayIdentifiers(false,false);
      CFIndex len=CFArrayGetCount(appIDs),i;
      struct app* info=malloc(sizeof(struct app)*len);
      for (i=0;i<len;i++){
        info[i].name=SBSCopyLocalizedApplicationNameForDisplayIdentifier(
         info[i].ID=CFArrayGetValueAtIndex(appIDs,i));
      }
      qsort(info,len,sizeof(struct app),$_compare);
      for (i=0;i<len;i++){
        char* _ID=$_copyUTF8String(info[i].ID);
        char* _name=$_copyUTF8String(info[i].name);
        CFRelease(info[i].name);
        printf("%s\t(%s)\n",_ID,_name);
        free(_ID);
        free(_name);
      }
      free(info);
      CFRelease(appIDs);
      return 0;
    }
    else if(opt=='q'){
      mach_port_t port=SBSSpringBoardServerPort();
      char _appID[1024]={0};
      if(optind<argc){
        CFStringRef appID=$_copyAppID(argv[optind]);
        CFStringGetCString(appID,_appID,1024,kCFStringEncodingUTF8); 
        CFRelease(appID);
      }
      else {SBFrontmostApplicationDisplayIdentifier(port,_appID);}
      fputs(_appID,stdout);
      if(_appID[0]){fputc('\n',stdout);}
      else {return 1;}
      SBBundlePathForDisplayIdentifier(port,_appID,buf);
      fputs(buf,stdout);
      if(buf[0]){
        fputc('\n',stdout);
        return 0;
      }
      return 1;
    }
    if(!optarg){return 1;}
    switch(opt){
      case 'o':_outFile=optarg;break;
      case 'e':_errFile=optarg;break;
      case 'u':
        if(openURL){CFRelease(openURL);}
        openURL=CFURLCreateWithBytes(NULL,(void*)optarg,strlen(optarg),kCFStringEncodingUTF8,NULL);
        break;
      default:
        fprintf(stderr,"W: Ignoring option -%c\n",opt);
        break;
    }
  }
  opt=optind;
  if(opt<argc){
    CFStringRef appID=$_copyAppID(argv[opt]);
    CFMutableArrayRef args=CFArrayCreateMutable(NULL,0,&kCFTypeArrayCallBacks);
    for (opt++;opt<argc;opt++){
      CFStringRef arg=CFStringCreateWithCStringNoCopy(NULL,argv[opt],kCFStringEncodingUTF8,kCFAllocatorNull);
      CFArrayAppendValue(args,arg);
      CFRelease(arg);
    }
    CFStringRef outFile=_outFile?$_copyPath(_outFile):NULL,
     errFile=_errFile?$_copyPath(_errFile):NULL;
    if(outFile && !errFile){errFile=CFRetain(outFile);}
    int error=SBSLaunchApplicationForDebugging(appID,openURL,args,NULL,outFile,errFile,4);
    if(outFile){CFRelease(outFile);}
    if(errFile){CFRelease(errFile);}
    if(openURL){CFRelease(openURL);}
    CFRelease(args);
    if(error){
      fputs("E: ",stderr);
      CFShow(SBSApplicationLaunchingErrorString(error));
      return error;
    }
    pid_t pid;
    while(!SBSProcessIDForDisplayIdentifier(appID,&pid)){usleep(100000);}
    printf("%u\n",pid);
    CFRelease(appID);
  }
  else if(openURL){
    if(_outFile){fputs("W: Ignoring option -o\n",stderr);}
    if(_errFile){fputs("W: Ignoring option -e\n",stderr);}
    NSAutoreleasePool* pool=[[NSAutoreleasePool alloc] init];
    SBSOpenSensitiveURLAndUnlock(openURL,true);
    [pool drain];
    CFRelease(openURL);
  }
  else {
    fprintf(stderr,"Usage: %s [-o stdout] [-e stderr] [-u URL] [appID] [arguments...]\n"
     "       %s [-l] [-q [appID]]\n",argv[0],argv[0]);
    return 1;
  }
  return 0;
}
