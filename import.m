#import <AVFoundation/AVFoundation.h>

@interface SSDownloadMetadata : NSObject
@property(assign) NSURL* primaryAssetURL;
@property(assign) NSDate* purchaseDate;
@property(assign) NSString* artistName;
@property(assign) NSString* collectionName;
@property(assign) NSString* composerName;
@property(assign) NSString* copyright;
@property(assign) NSURL* fullSizeImageURL;
@property(assign) NSString* genre;
@property(assign) NSString* shortDescription;
@property(assign) NSString* releaseDateString;
@property(assign) NSString* title;
@property(assign,getter=isCompilation) BOOL compilation;
@property(assign,getter=isExplicitContent) BOOL explicitContent;
@property(assign) NSString* collectionArtistName;
@property(assign) NSNumber* collectionIndexInCollectionGroup;
@property(assign) NSNumber* numberOfCollectionsInCollectionGroup;
@property(assign) NSNumber* indexInCollection;
@property(assign) NSNumber* numberOfItemsInCollection;
@property(assign) NSString* longDescription;
@property(assign) NSString* longSeasonDescription;
@property(assign) NSNumber* releaseYear;
@property(assign) NSString* subtitle;
@property(assign) NSString* episodeIdentifier;
@property(assign) NSString* networkName;
@property(assign) NSNumber* seasonNumber;
@property(assign) NSString* seriesName;
-(id)initWithKind:(NSString*)kind;
@end

@interface SSDownload : NSObject
-(id)initWithDownloadMetadata:(SSDownloadMetadata*)metadata;
-(long long)persistentIdentifier;
@end

@interface SSDownloadQueue : NSObject
+(NSArray*)mediaDownloadKinds;
-(id)initWithDownloadKinds:(NSArray*)kinds;
-(BOOL)addDownload:(SSDownload*)download;
@end

static void $_addDownload(SSDownloadQueue* queue,NSURL* URL,NSString* kind) {
  static NSString* genres[]={nil,
   @"Blues",@"Classic Rock",@"Country",@"Dance",@"Disco",@"Funk",
   @"Grunge",@"Hip-Hop",@"Jazz",@"Metal",@"New Age",@"Oldies",
   @"Other",@"Pop",@"R&B",@"Rap",@"Reggae",@"Rock",
   @"Techno",@"Industrial",@"Alternative",@"Ska",@"Death Metal",@"Pranks",
   @"Soundtrack",@"Euro-Techno",@"Ambient",@"Trip-Hop",@"Vocal",@"Jazz+Funk",
   @"Fusion",@"Trance",@"Classical",@"Instrumental",@"Acid",@"House",
   @"Game",@"Sound Clip",@"Gospel",@"Noise",@"AlternRock",@"Bass",
   @"Soul",@"Punk",@"Space",@"Meditative",@"Instrumental Pop",@"Instrumental Rock",
   @"Ethnic",@"Gothic",@"Darkwave",@"Techno-Industrial",@"Electronic",@"Pop-Folk",
   @"Eurodance",@"Dream",@"Southern Rock",@"Comedy",@"Cult",@"Gangsta",
   @"Top 40",@"Christian Rap",@"Pop/Funk",@"Jungle",@"Native American",@"Cabaret",
   @"New Wave",@"Psychadelic",@"Rave",@"Showtunes",@"Trailer",@"Lo-Fi",
   @"Tribal",@"Acid Punk",@"Acid Jazz",@"Polka",@"Retro",@"Musical",
   @"Rock & Roll",@"Hard Rock",@"Folk",@"Folk-Rock",@"National Folk",@"Swing",
   @"Fast Fusion",@"Bebob",@"Latin",@"Revival",@"Celtic",@"Bluegrass",
   @"Avantgarde",@"Gothic Rock",@"Progressive Rock",@"Psychedelic Rock",@"Symphonic Rock",@"Slow Rock",
   @"Big Band",@"Chorus",@"Easy Listening",@"Acoustic",@"Humour",@"Speech",
   @"Chanson",@"Opera",@"Chamber Music",@"Sonata",@"Symphony",@"Booty Bass",
   @"Primus",@"Porn Groove",@"Satire",@"Slow Jam",@"Club",@"Tango",
   @"Samba",@"Folklore",@"Ballad",@"Power Ballad",@"Rhythmic Soul",@"Freestyle",
   @"Duet",@"Punk Rock",@"Drum Solo",@"A capella",@"Euro-House",@"Dance Hall"};
  NSNumber* value=nil;
  [URL getResourceValue:&value forKey:NSURLIsDirectoryKey error:NULL];
  if(value.boolValue){return;}
  AVAsset* asset=[AVAsset assetWithURL:URL];
  if(!asset.playable){return;}
  NSString* ext=URL.pathExtension;
  unsigned long pre=URL.hash,counter=0;
  BOOL ilink=YES;
  NSURL* tmpURL;
  NSError* error;
  NSFileManager* manager=[NSFileManager defaultManager];
  while(1){
    tmpURL=[NSURL fileURLWithPath:[NSString stringWithFormat:
     @"/tmp/import-%lu-%lu.%@",pre,counter,ext]];
    if(ilink?[manager linkItemAtURL:URL toURL:tmpURL error:&error]:
     [manager copyItemAtURL:URL toURL:tmpURL error:&error]){break;}
    tmpURL=nil;
    error=[error.userInfo objectForKey:NSUnderlyingErrorKey];
    if(![error.domain isEqualToString:NSPOSIXErrorDomain]){break;}
    NSInteger code=error.code;
    if(code==EEXIST){counter++;}
    else if(code==EXDEV && ilink){ilink=NO;}
    else {break;}
  }
  if(!tmpURL){
    fprintf(stderr,"E: %s\n",error.localizedDescription.UTF8String);
    return;
  }
  SSDownloadMetadata* metadata=[[SSDownloadMetadata alloc] initWithKind:kind];
  metadata.primaryAssetURL=tmpURL;
  metadata.purchaseDate=[NSDate date];
  NSData* artworkData=nil;
  for (AVMetadataItem* item in asset.commonMetadata){
    NSString* key=item.commonKey;
    if([key isEqualToString:AVMetadataCommonKeyArtist]){
      metadata.artistName=item.stringValue;
    }
    else if([key isEqualToString:AVMetadataCommonKeyAlbumName]){
      metadata.collectionName=item.stringValue;
    }
    else if([key isEqualToString:AVMetadataCommonKeyCreator]){
      metadata.composerName=item.stringValue;
    }
    else if([key isEqualToString:AVMetadataCommonKeyCopyrights]){
      metadata.copyright=item.stringValue;
    }
    else if([key isEqualToString:AVMetadataCommonKeyArtwork]){
      artworkData=[item.keySpace isEqualToString:AVMetadataKeySpaceID3]?
       [(NSDictionary*)item.value objectForKey:@"data"]:item.dataValue;
    }
    else if([key isEqualToString:AVMetadataCommonKeyType]){
      metadata.genre=[item.value isKindOfClass:[NSData class]]?
       genres[CFSwapInt16(*(unsigned short*)item.dataValue.bytes)]:
       item.stringValue;
    }
    else if([key isEqualToString:AVMetadataCommonKeyCreationDate]){
      metadata.releaseDateString=item.stringValue;
    }
    else if([key isEqualToString:AVMetadataCommonKeyDescription]){
      metadata.shortDescription=item.stringValue;
    }
    else if([key isEqualToString:AVMetadataCommonKeyTitle]){
      metadata.title=item.stringValue;
    }
  }
  for (NSString* format in asset.availableMetadataFormats){
    for (AVMetadataItem* item in [asset metadataForFormat:format]){
      NSString* key=(NSString*)item.key;
      if([key isKindOfClass:[NSNumber class]]){
        unsigned char value[4];
        *(unsigned int*)value=[(NSNumber*)key unsignedIntValue];
        key=[NSString stringWithFormat:@"%c%c%c%c",
         (value[3]==169)?'@':value[3],value[2],value[1],value[0]];
      }
      NSString* keySpace=item.keySpace;
      if([keySpace isEqualToString:AVMetadataKeySpaceID3]){
        if([key isEqualToString:@"TCMP"]){
          metadata.compilation=item.numberValue.boolValue;
        }
        else if([key isEqualToString:AVMetadataID3MetadataKeyBand]){
          metadata.collectionArtistName=item.stringValue;
        }
        else if([key isEqualToString:AVMetadataID3MetadataKeyPartOfASet]){
          NSArray* value=[item.stringValue componentsSeparatedByString:@"/"];
          metadata.collectionIndexInCollectionGroup=[NSNumber
           numberWithInt:[[value objectAtIndex:0] intValue]];
          if(value.count>1){
            metadata.numberOfCollectionsInCollectionGroup=[NSNumber
             numberWithInt:[[value objectAtIndex:1] intValue]];
          }
        }
        else if([key isEqualToString:AVMetadataID3MetadataKeyTrackNumber]){
          NSArray* value=[item.stringValue componentsSeparatedByString:@"/"];
          metadata.indexInCollection=[NSNumber
           numberWithInt:[[value objectAtIndex:0] intValue]];
          if(value.count>1){
            metadata.numberOfItemsInCollection=[NSNumber
             numberWithInt:[[value objectAtIndex:1] intValue]];
          }
        }
        else if([key isEqualToString:AVMetadataID3MetadataKeyComments]){
          NSDictionary* value=(NSDictionary*)item.value;
          if([[value objectForKey:@"identifier"] length]==0){
            metadata.longDescription=[value objectForKey:@"text"];
          }
        }
        else if([key isEqualToString:AVMetadataID3MetadataKeyContentGroupDescription]){
          metadata.longSeasonDescription=item.stringValue;
        }
        else if([key isEqualToString:AVMetadataID3MetadataKeyYear]){
          metadata.releaseYear=item.numberValue;
        }
        else if([key isEqualToString:AVMetadataID3MetadataKeySubTitle]){
          metadata.subtitle=item.stringValue;
        }
      }
      else if([keySpace isEqualToString:AVMetadataKeySpaceiTunes]){
        if([key isEqualToString:AVMetadataiTunesMetadataKeyDiscCompilation]){
          metadata.compilation=item.numberValue.boolValue;
        }
        else if([key isEqualToString:AVMetadataiTunesMetadataKeyContentRating]){
          metadata.explicitContent=(item.numberValue.intValue==4);
        }
        else if([key isEqualToString:AVMetadataiTunesMetadataKeyAlbumArtist]){
          metadata.collectionArtistName=item.stringValue;
        }
        if([key isEqualToString:AVMetadataiTunesMetadataKeyUserGenre]){
          metadata.genre=item.stringValue;
        }
        else if([key isEqualToString:AVMetadataiTunesMetadataKeyDiscNumber]){
          short* value=(short*)item.dataValue.bytes;
          metadata.collectionIndexInCollectionGroup=[NSNumber
           numberWithShort:CFSwapInt16(value[1])];
          metadata.numberOfCollectionsInCollectionGroup=[NSNumber
           numberWithShort:CFSwapInt16(value[2])];
        }
        else if([key isEqualToString:AVMetadataiTunesMetadataKeyTrackNumber]){
          short* value=(short*)item.dataValue.bytes;
          metadata.indexInCollection=[NSNumber
           numberWithShort:CFSwapInt16(value[1])];
          metadata.numberOfItemsInCollection=[NSNumber
           numberWithShort:CFSwapInt16(value[2])];
        }
        else if([key isEqualToString:AVMetadataiTunesMetadataKeyUserComment]
         || [key isEqualToString:@"ldes"]){
          metadata.longDescription=item.stringValue;
        }
        else if([key isEqualToString:AVMetadataiTunesMetadataKeyTrackSubTitle]){
          metadata.subtitle=item.stringValue;
        }
        else if([key isEqualToString:@"tven"]){
          metadata.episodeIdentifier=item.stringValue;
        }
        else if([key isEqualToString:@"tvnn"]){
          metadata.networkName=item.stringValue;
        }
        else if([key isEqualToString:@"tvsn"]){
          metadata.seasonNumber=item.numberValue;
        }
        else if([key isEqualToString:@"tvsh"]){
          metadata.seriesName=item.stringValue;
        }
      }
    }
  }
  if(!metadata.title){
    metadata.title=URL.lastPathComponent.stringByDeletingPathExtension;
  }
  if(artworkData){
    char tmpfn[]="/tmp/artwork-XXXXXXXX";
    NSFileHandle* handle=[[NSFileHandle alloc]
     initWithFileDescriptor:mkstemp(tmpfn) closeOnDealloc:YES];
    [handle writeData:artworkData];
    [handle release];
    [metadata.fullSizeImageURL=(NSURL*)CFURLCreateFromFileSystemRepresentation(NULL,
     (const UInt8*)tmpfn,strlen(tmpfn),false) release];
  }
  SSDownload* download=[[SSDownload alloc] initWithDownloadMetadata:metadata];
  [metadata release];
  if([queue addDownload:download]){
    printf("%lld\t%s\n",download.persistentIdentifier,
     URL.path.fileSystemRepresentation);
  }
  [download release];
}

int main(int argc,char** argv) {
  NSAutoreleasePool* pool=[[NSAutoreleasePool alloc] init];
  NSArray* kinds=[SSDownloadQueue mediaDownloadKinds];
  if(argc<2){
    printf("Usage: %s [[scheme:]path...]\n\nSchemes:\n",argv[0]);
    for (NSString* kind in kinds){
      printf("  %s\n",kind.UTF8String);
    }
    return 1;
  }
  SSDownloadQueue* queue=[[SSDownloadQueue alloc] initWithDownloadKinds:kinds];
  int i;
  for (i=1;i<argc;i++){
    NSString* kind;
    char* fn=argv[i],*sep=strchr(fn,':');
    if(sep){
      sep[0]=0;
      kind=[NSString stringWithUTF8String:fn];
      fn=sep+1;
    }
    else {kind=@"song";}
    NSURL* URL=(NSURL*)CFURLCreateFromFileSystemRepresentation(NULL,
     (const UInt8*)fn,strlen(fn),false);
    $_addDownload(queue,URL,kind);
    for (NSURL* subURL in [[NSFileManager defaultManager]
     enumeratorAtURL:URL includingPropertiesForKeys:
     [NSArray arrayWithObject:NSURLIsDirectoryKey]
     options:0 errorHandler:NULL]){
      $_addDownload(queue,subURL,kind);
    }
    [URL release];
  }
  [queue release];
  [pool drain];
}
