#import <AVFoundation/AVFoundation.h>

#define OPT_NLOOPS 1
#define OPT_GAIN 2
#define OPT_PAN 4

@interface AudioPlayerDelegate : NSObject @end
@implementation AudioPlayerDelegate
+(void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer*)player error:(NSError*)error {
  fprintf(stderr,"E[AudioPlayerDelegate]: %s\n",error.localizedDescription.UTF8String);
}
+(void)audioPlayerDidFinishPlaying:(AVAudioPlayer*)player successfully:(BOOL)success {
  CFRunLoopStop(CFRunLoopGetMain());
}
@end

int main(int argc,char** argv) {
  char options=0;
  float p_gain,p_pan;
  int p_nloops,opt;
  while((opt=getopt(argc,argv,"n:g:p:"))!=-1){
    if(!optarg){return 1;}
    char ierr=(opt=='n' && (options|=OPT_NLOOPS))?(sscanf(optarg,"%d",&p_nloops)!=1):
     (opt=='g' && (options|=OPT_GAIN))?(sscanf(optarg,"%f",&p_gain)!=1 || p_gain<0 || p_gain>1):
     (opt=='p' && (options|=OPT_PAN))?(sscanf(optarg,"%f",&p_pan)!=1 || p_pan<-1 || p_pan>1):2;
    if(ierr==2){fprintf(stderr,"Warning: Ignoring option -%c\n",opt);}
    else if(ierr){
      fprintf(stderr,"-%c: Invalid argument\n",opt);
      return 1;
    }
  }
  if(optind>=argc){
    fprintf(stderr,"Usage: %s [-n nloops] [-g gain(0..1)] [-p pan(-1..1)] <file>\n",argv[0]);
    return 1;
  }
  NSAutoreleasePool* pool=[[NSAutoreleasePool alloc] init];
  AVAudioSession* session=[AVAudioSession sharedInstance];
  NSError* error;
  if(![session setCategory:AVAudioSessionCategoryPlayback error:&error]){
    fprintf(stderr,"E[AVAudioSession]: %s\n",error.localizedDescription.UTF8String);
  }
  if(![session setActive:YES error:&error]){
    fprintf(stderr,"E[AVAudioSession]: %s\n",error.localizedDescription.UTF8String);
  }
  for (opt=optind;opt<argc;opt++){
    char* infn=argv[opt];
    NSURL* URL=[NSURL fileURLWithPath:[[NSFileManager defaultManager]
     stringWithFileSystemRepresentation:infn length:strlen(infn)]];
    printf("Playing [%s]...\n",URL.path.UTF8String);
    AVAudioPlayer* player=[[AVAudioPlayer alloc] initWithContentsOfURL:URL error:&error];
    if(!player){
      fprintf(stderr,"E[AVAudioPlayer]: %s\n",error.localizedDescription.UTF8String);
      continue;
    }
    player.delegate=(id)[AudioPlayerDelegate class];
    if(options&OPT_NLOOPS){player.numberOfLoops=p_nloops;}
    if(options&OPT_GAIN){player.volume=p_gain;}
    if(options&OPT_PAN){player.pan=p_pan;}
    [player play];
    CFRunLoopRun();
    [player stop];
    [player release];
  }
  [pool drain];
}
