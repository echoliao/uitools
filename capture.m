#import <MobileCoreServices/UTCoreTypes.h>
#import <ImageIO/CGImageDestination.h>
#include "capture.h"

int main() {
  NSAutoreleasePool* pool=[[NSAutoreleasePool alloc] init];
  CGRect frame=[CADisplay mainDisplay].bounds;
  int W=frame.size.width,H=frame.size.height;
  IOSurfaceRef surface=IOSurfaceCreate(
   [NSDictionary dictionaryWithObjectsAndKeys:
   [NSNumber numberWithInt:kCVPixelFormatType_32BGRA],kIOSurfacePixelFormat,
   [NSNumber numberWithInt:W],kIOSurfaceWidth,
   [NSNumber numberWithInt:H],kIOSurfaceHeight,
   [NSNumber numberWithInt:4],kIOSurfaceBytesPerElement,
   [NSNumber numberWithInt:4*W],kIOSurfaceBytesPerRow,
   [NSNumber numberWithInt:4*W*H],kIOSurfaceAllocSize,
   [NSNumber numberWithBool:YES],kIOSurfaceIsGlobal,nil]);
  IOSurfaceLock(surface,0,NULL);
  CARenderServerRenderDisplay(0,CFSTR("LCD"),surface,0,0);
  IOSurfaceUnlock(surface,0,NULL);
  CGDataProviderRef provider=CGDataProviderCreateWithData(
   NULL,IOSurfaceGetBaseAddress(surface),4*W*H,NULL);
  CGColorSpaceRef cspace=CGColorSpaceCreateDeviceRGB();
  CGImageRef img=CGImageCreate(W,H,8,8*4,4*W,cspace,
   kCGImageAlphaNoneSkipFirst|kCGBitmapByteOrder32Little,
   provider,NULL,true,kCGRenderingIntentDefault);
  CFRelease(cspace);
  CFRelease(provider);
  CFMutableDataRef data=NULL;
  CGImageDestinationRef dest;
  if(isatty(fileno(stdout))){
    const char* fn="screen.png";
    CFURLRef URL=CFURLCreateFromFileSystemRepresentation(
     NULL,(const UInt8*)fn,strlen(fn),false);
    dest=CGImageDestinationCreateWithURL(URL,kUTTypePNG,1,NULL);
    CFRelease(URL);
  }
  else {
    data=CFDataCreateMutable(NULL,0);
    dest=CGImageDestinationCreateWithData(data,kUTTypePNG,1,NULL);
  }
  CGImageDestinationAddImage(dest,img,NULL);
  CGImageDestinationFinalize(dest);
  CFRelease(dest);
  CFRelease(img);
  CFRelease(surface);
  if(data){
    fwrite(CFDataGetBytePtr(data),sizeof(UInt8),CFDataGetLength(data),stdout);
    CFRelease(data);
  }
  [pool drain];
  return 0;
}
