int main(int argc,char** argv) {
  NSAutoreleasePool* pool=[[NSAutoreleasePool alloc] init];
  [[NSFileHandle fileHandleWithStandardOutput]
   writeData:[[UIPasteboard generalPasteboard]
   dataForPasteboardType:@"public.data"]];
  [pool drain];
}
