@interface SSPurchaseRequest : NSObject
-(id)initWithPurchases:(NSArray*)purchases;
-(BOOL)start;
@end

@interface SSPurchase
+(SSPurchase*)purchaseWithBuyParameters:(NSString*)params;
@end

@interface SSAccount
-(NSString*)accountName;
-(NSString*)storeFrontIdentifier;
-(NSNumber*)uniqueIdentifier;
@end

@interface SSAccountStore
+(SSAccountStore*)defaultStore;
-(SSAccount*)activeAccount;
@end

@interface ISCookieStorage
+(ISCookieStorage*)sharedInstance;
-(NSDictionary*)cookieHeadersForURL:(NSURL*)URL userIdentifier:(NSNumber*)identifier;
@end

@interface ISURLOperation
+(NSString*)copyUserAgent;
@end

int main(int argc,char** argv) {
  NSAutoreleasePool* pool=[[NSAutoreleasePool alloc] init];
  if(argc>1){
    NSMutableArray* purchases=[NSMutableArray array];
    int i;
    for (i=1;i<argc;i++){
      const char* pos=argv[i]+strcspn(argv[i],"0123456789");
      size_t span=strspn(pos,"0123456789");
      if(span){
        fwrite(pos,1,span,stdout);
        fputc('\n',stdout);
        NSString* ID=[[NSString alloc] initWithBytesNoCopy:(void*)pos
         length:span encoding:NSUTF8StringEncoding freeWhenDone:NO];
        [purchases addObject:[SSPurchase purchaseWithBuyParameters:
         [@"productType=C&price=0&pricingParameters=STDQ&salableAdamId="
         stringByAppendingString:ID]]];
        [ID release];
      }
    }
    if(!purchases.count){
      fprintf(stderr,"Usage: %s [iTunesID...]\n",argv[0]);
      return 1;
    }
    SSPurchaseRequest* req=[[SSPurchaseRequest alloc] initWithPurchases:purchases];
    [req start];
    [req release];
    return 0;
  }
  SSAccount* account=[SSAccountStore defaultStore].activeAccount;
  if(!account){
    fputs("E: Not signed into iTunes\n",stderr);
    return 1;
  }
  NSNumber* dsid=account.uniqueIdentifier;
  fprintf(stderr,"%s (%llu) %s\n",account.accountName.UTF8String,
   dsid.unsignedLongLongValue,account.storeFrontIdentifier.UTF8String);
  NSURL* URL=[NSURL URLWithString:@"https://se.itunes.apple.com/WebObjects/MZStoreElements.woa/wa/purchases?mt=8"];
  NSMutableURLRequest* req=[NSMutableURLRequest requestWithURL:URL];
  NSDictionary* headers=[[ISCookieStorage sharedInstance]
   cookieHeadersForURL:URL userIdentifier:dsid];
  for (NSString* field in headers){
    [req addValue:[headers objectForKey:field] forHTTPHeaderField:field];
  }
  NSString* agent=[ISURLOperation copyUserAgent];
  [req addValue:agent forHTTPHeaderField:@"User-Agent"];
  [agent release];
  const char* pos=strstr([NSURLConnection sendSynchronousRequest:req
   returningResponse:NULL error:NULL].bytes,"\"contentIds\":[");
  if(!pos){
    fputs("E: Missing data in response\n",stderr);
    return 2;
  }
  pos+=14;
  while(1){
    size_t span=strspn(pos,"0123456789");
    if(span){
      fwrite(pos,1,span,stdout);
      fputc('\n',stdout);
      pos+=span;
    }
    else if(*pos==' ' || *pos==','){pos++;}
    else {break;}
  }
  [pool drain];
}
